ARG PHP_VERSION=""
FROM php:${PHP_VERSION:+${PHP_VERSION}-}fpm-alpine
RUN apk update; \
    apk upgrade;

# install mysql & mysqli php extensions
RUN docker-php-ext-install mysql
RUN docker-php-ext-install mysqli

# install gd php extension
RUN apk add --no-cache freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev && \
   docker-php-ext-configure gd \
   --with-gd \
   --with-freetype-dir=/usr/include/ \
   --with-png-dir=/usr/include/ \
   --with-jpeg-dir=/usr/include/ && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  docker-php-ext-install -j${NPROC} gd && \
  apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

# install phpredis php extension
ARG PHPREDIS_VERSION
RUN apk add --no-cache \
        $PHPIZE_DEPS \
    && pecl install redis-$PHPREDIS_VERSION \
    && docker-php-ext-enable redis

# configure redis for php session
ARG REDIS_PATH
RUN echo 'session.save_handler = redis' >> /usr/local/etc/php/conf.d/docker-php-ext-redis.ini
RUN echo session.save_path = "$REDIS_PATH" >> /usr/local/etc/php/conf.d/docker-php-ext-redis.ini

# install ldap php extension
RUN apk add --no-cache --virtual .persistent-deps \
		libldap
RUN apk update && \
         apk add --no-cache --virtual .build-deps $PHPIZE_DEPS openldap-dev && \
         docker-php-ext-install ldap && \
         apk del .build-deps

# configure display error
RUN echo "php_admin_value[error_reporting] = E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED" >> /usr/local/etc/php-fpm.d/www.conf
RUN echo "php_flag[display_errors] = on" >> /usr/local/etc/php-fpm.d/www.conf
RUN echo "php_flag[display_startup_errors] = on" >> /usr/local/etc/php-fpm.d/www.conf
